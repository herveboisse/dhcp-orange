
option auth code 90 = string;
option vendor-info code 125 = { integer 32, integer 8, string };

option dhcp6.auth code 11 = string;
option dhcp6.user-class code 15 = { integer 16, text };
option dhcp6.vendor-class code 16 = { integer 32, integer 16, text };

request
	subnet-mask,
	routers,
	domain-name-servers,
	domain-name,
	broadcast-address,
	dhcp-lease-time,
	dhcp-renewal-time,
	dhcp-rebinding-time,
	auth,
	domain-search,
	vendor-info,
	dhcp6.auth,
	dhcp6.vendor-opts,
	dhcp6.name-servers,
	dhcp6.domain-search;

send vendor-class-identifier "sagem";
send dhcp-client-identifier = hardware;
send user-class "\x2bFSVDSL_livebox.Internet.softathome.LB_VERSION";
send auth AUTH;

send dhcp6.auth AUTH;
send dhcp6.user-class 43 "FSVDSL_livebox.Internet.softathome.LB_VERSION";
send dhcp6.vendor-class 1038 5 "sagem";

