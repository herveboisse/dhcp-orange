#!/bin/sh
# shellcheck disable=SC3043 # use of "local" keyword
exec </dev/null >&2
set -e
#set -x

TOPDIR="$(dirname "${0}")"
TOPDIR="$(readlink -f "${TOPDIR}")"

: "${DHCP_ORANGE_CONF:="/etc/dhcp-orange/dhcp-orange.conf"}"
. "${DHCP_ORANGE_CONF}"

# generated conf file contains credential data
# prevent it from being world readable
umask 0077


syslog () {
	local level="info"

	if [ ${#} -ge 2 ]; then
		level="${1:-info}"
		shift
	fi

	#echo "dhcp-orange[${$}]: ${iface}: ${*}"; return 0
	echo "${iface}: ${*}" |\
		logger -p syslog.${level} -t "dhcp-orange[${$}]"
}

setup_cos () {
	# egress hooks can only be created for already existing interfaces
	${NFT} -f - <<EOF
table netdev firewall {
	chain orange-${iface} {
		type filter hook egress device ${iface} priority filter
		policy accept

		goto prio_orange
	}
}
EOF
}

gen_conf () {
	local auth

	auth="$("${TOPDIR}/gen_auth_orange.sh" |\
		hexdump -v -e '1/1 "%02x:"' |\
		sed 's/.$/\n/')"

	sed \
		-e "s/\\<AUTH\\>/${auth}/g" \
		-e "s/\\<LB_VERSION\\>/${LB_VERSION}/g" \
		"${TOPDIR}/dhclient-orange.conf.tpl" \
		> "/tmp/dhclient-orange-${iface}.conf"
}

start4 () {
	${DHCLIENT} \
		-4 \
		-nw \
		-cf "/tmp/dhclient-orange-${iface}.conf" \
		-pf "/var/run/dhclient.${iface}.pid" \
		-lf /dev/null \
		"${iface}"
}

stop4 () {
	${DHCLIENT} \
		-4 \
		-r \
		-cf "/tmp/dhclient-orange-${iface}.conf" \
		-pf "/var/run/dhclient.${iface}.pid" \
		"${iface}" \
		|| true
	rm -f "/var/run/dhclient.${iface}.pid"

	# avoid any remnant IPv4
	${IP} -4 addr flush dev "${iface}"
}

check4 () {
	local addr

	addr="$(${IP} -o -4 route list dev "${iface}" |\
		sed -n '/^default\s/!b; s/^.*\<via\s\+//; s/\s.*$//; p; q')"
	if [ -z "${addr}" ]; then
		syslog "no IPv4 router"
		return 1
	fi

	${ARPING} \
		-qfb \
		-c 1 \
		-w 1 \
		-I "${iface}" \
		"${addr}" \
		>/dev/null \
		|| return 1
}

start6 () {
	${DHCLIENT} \
		-6 \
		-P \
		-D LL \
		-nw \
		-cf "/tmp/dhclient-orange-${iface}.conf" \
		-pf "/var/run/dhclient6.${iface}.pid" \
		-lf /dev/null \
		"${iface}"
}

stop6 () {
	${DHCLIENT} \
		-6 \
		-r \
		-cf "/tmp/dhclient-orange-${iface}.conf" \
		-pf "/var/run/dhclient6.${iface}.pid" \
		"${iface}" \
		|| true
	rm -f "/var/run/dhclient6.${iface}.pid"

	# avoid any remnant IPv6
	${IP} -6 addr flush scope global to 2000::/3 dev "${iface}"
}

check6 () {
	local addr

	addr="$(${IP} -o -6 route list dev "${iface}" proto ra |\
		sed -n '/^default\s/!b; s/^.*\<via\s\+//; s/\s.*$//; p; q')"
	if [ -z "${addr}" ]; then
		syslog "no IPv6 router"
		return 1
	fi

	"${NDISC6}" \
		-1qn \
		-r 1 \
		-w 1000 \
		"${addr}" \
		"${iface}" \
		>/dev/null \
		|| return 1
}

cleanup_hook () {
	syslog "stop requested"

	stop4
	stop6
	rm -f "/tmp/dhclient-orange-${iface}.conf"

	syslog "stopped"

	exit 0
}


if [ -z "${1}" ]; then
	echo "usage: ${0} <interface>"
	exit 1
fi
iface="${1}"

trap cleanup_hook EXIT
trap true INT TERM

syslog "starting"

setup_cos

while ! ${IP} -o -6 addr show dev "${iface}" scope link -tentative |\
		grep -q '^.'; do
	syslog "waiting for valid IPv6 link-local address"
	sleep 1
done

gen_conf
start4
start6
# shellcheck disable=SC2034 # unused variable
count4=0
# shellcheck disable=SC2034 # unused variable
count6=0

while true; do
	sleep ${INTERVAL} &
	wait ${!}

	for f in 4 6; do
		count=0
		eval "count${f}=\$(( count${f} + 1 )); count=\${count${f}}"
		if check${f}; then
			if [ ${count} -gt 1 ]; then
				syslog "IPv${f} router back at check attempt ${count}"
			fi
			eval "count${f}=0"
		elif [ ${count} -lt ${MAXFAIL} ]; then
			syslog "IPv${f} router check attempt ${count} failed"
		else
			syslog "IPv${f} router dead after ${count} attempts"

			stop${f}

			gen_conf
			start${f}
			eval "count${f}=0"
		fi
	done
done

