#!/bin/sh
set -e

TOPDIR="$(dirname "${0}")"
TOPDIR="$(readlink -f "${TOPDIR}")"

: "${DHCP_AUTH_FILE:="${TOPDIR}/auth.conf"}"


# Orange authentication format:
#
# - 11 null bytes: the unused part of RFC 3118
#
# the next bytes are sequence of TLVs (1 byte tag, 1 byte length, variable value):
# - constant pattern: T = 1a, V = 00000558010341
# - username: T = 01, V = <username>
# - randstring: T = 3c, V = <16 random bytes>
# - auth: T = 03, V = <randchar: 1 random byte> + md5(randchar + password + randstring)
#
# total size = 70 bytes (assuming all usernames are constant size = 11 chars)

read -r user pass < "${DHCP_AUTH_FILE}"

if [ ${#user} != 11 ]; then
	echo "invalid user" >&2
	exit 1
fi

head -c 11 /dev/zero

env printf "\x1a\x09\x00\x00\x05\x58\x01\x03\x41"

env printf "\x01\x0d%s" "${user}"

s="$(tr -d -c 'a-zA-Z0-9@[]^#!(){},`;.:&*-' </dev/urandom 2>/dev/null |head -c 16)"
c="$(tr -d -c 'a-zA-Z0-9' </dev/urandom 2>/dev/null |head -c 1)"

env printf "\x3c\x12%s" "${s}"

env printf "\x03\x13%c" "${c}"
env printf "%c%s%s" "${c}" "${pass}" "${s}" |openssl md5 -binary

