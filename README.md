
## Orange DHCP configuration

This repo contains a basic Linux configuration to connect to Orange Internet.  
This setup is intended to be used on Alpine Linux distro, but can be adapted for other systems.  
The files here are NOT intended to be used as a ready "drop in" configuration, just as a base example to start with.  

Remember to modify `/etc/dhcp/auth.conf` with your proper credential information (and set the file permissions to 0600 to prevent world readability).  

**Important notice**: except for IPv6 WAN address which is set dynamically, IPv6 prefix is considered static and LAN configuration should be set in both `/etc/network/interfaces` and `/etc/radvd.conf` (not included in this example).  

Dependencies:

| | standalone | busybox |
| --- | --- | --- |
| shell | :heavy_check_mark: (bash)<br> :heavy_check_mark: (dash) | :heavy_check_mark: (ash) |
| ifupdown-ng | :heavy_check_mark:<br> :grey_question: (Debian) | :grey_question: |
| coreutils | :heavy_check_mark: | :grey_question: |
| logger | :heavy_check_mark: (util-linux) | :grey_question: |
| grep | :heavy_check_mark: | :grey_question: |
| sed | :heavy_check_mark: | :grey_question: |
| hexdump | :heavy_check_mark: | :grey_question: |
| ip | :heavy_check_mark: (iproute2) | :grey_question: |
| nftables<sup>1</sup> | :heavy_check_mark: | N/A |
| start-stop-daemon | :heavy_check_mark: (OpenRC)<br> :grey_question: (Debian) | :grey_question: |
| ISC dhclient<sup>2</sup> | :heavy_check_mark: | N/A |
| udhcpc[46] | N/A | :x: TODO |
| arping | :heavy_check_mark: (iputils)<br> :x: (Thomas Habets) | :heavy_check_mark: |
| ndisc6 | :heavy_check_mark: | N/A |
| openssl (command line tool) | :heavy_check_mark: | N/A |

:heavy_check_mark: ok  
:grey_question: not tested but should work  
:x: does NOT work  

<sup>1</sup>: the firewall rules only cover the needed packet modification rules, this is NOT a complete firewall setup  
<sup>2</sup>: only with versions <= 4.4.3, this client is deprecated, next release will be the last one and will remove client support  
Important: starting from Alpine Linux v3.21, support for ISC DHCP was completely removed.  
As a response I am packaging it back (though support for client only) in my own Alpine repository [here](http://arepo.netgeek.ovh/).  

